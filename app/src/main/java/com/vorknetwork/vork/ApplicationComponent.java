package com.vorknetwork.vork;


import com.vorknetwork.vork.ui.activity.LoginActivity;
import com.vorknetwork.vork.ui.activity.SplashActivity;
import com.vorknetwork.vork.ui.main.MainActivityComponent;
import com.vorknetwork.vork.ui.main.MainActivityModule;

import dagger.Component;

@Component(
        modules = {
                ApplicationModule.class
        })
@DaggerScope(ApplicationComponent.class)
public interface ApplicationComponent {

    MainActivityComponent join(MainActivityModule module);

    void inject(VorkController controller);

    void inject(LoginActivity activity);

    void inject(SplashActivity activity);
}
