package com.vorknetwork.vork;


import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vorknetwork.vork.service.IBroadcast;
import com.vorknetwork.vork.service.IImageLoader;
import com.vorknetwork.vork.service.impl.EventBusBroadcastImpl;
import com.vorknetwork.vork.service.impl.PicassoImageLoaderImpl;

import org.greenrobot.eventbus.EventBus;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private Context context;

    public ApplicationModule(Context context) {
        this.context = context;
    }

    @Provides
    @DaggerScope(ApplicationComponent.class)
    public IBroadcast provideBroadcast() {
        return new EventBusBroadcastImpl(EventBus.getDefault());
    }

    @Provides
    @DaggerScope(ApplicationComponent.class)
    public IImageLoader provideImageLoader() {
        return new PicassoImageLoaderImpl(context);
    }

    @Provides
    @DaggerScope(ApplicationComponent.class)
    public Gson provideGson() {
        return new GsonBuilder().create();
    }
}
