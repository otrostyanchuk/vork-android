package com.vorknetwork.vork.flow;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.vorknetwork.vork.R;
import com.vorknetwork.vork.ui.main.MainPathContainer;

import flow.Flow;
import flow.path.Path;
import flow.path.PathContainer;
import flow.path.PathContainerView;

public abstract class FramePathContainerView extends FrameLayout
        implements HandlesBack, PathContainerView {
    private PathContainer container;
    private boolean disabled;

    public FramePathContainerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return !disabled && super.dispatchTouchEvent(ev);
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override
    public ViewGroup getContainerView() {
        return this;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        container = getContainer();
        if (container == null) {
            container = new MainPathContainer(R.id.screen_switcher_tag, Path.contextFactory(), null);
        }
    }

    @Override
    public void dispatch(Flow.Traversal traversal, final Flow.TraversalCallback callback) {
        disabled = true;
        container.executeTraversal(this, traversal, new Flow.TraversalCallback() {
            @Override
            public void onTraversalCompleted() {
                callback.onTraversalCompleted();
                disabled = false;
            }
        });
    }

    @Override
    public boolean onBackPressed() {
        return BackSupport.onBackPressed(getCurrentChild());
    }

    @Override
    public ViewGroup getCurrentChild() {
        return (ViewGroup) getContainerView().getChildAt(0);
    }

    protected abstract PathContainer getContainer();
}
