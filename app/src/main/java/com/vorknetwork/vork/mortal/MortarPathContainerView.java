package com.vorknetwork.vork.mortal;

import android.content.Context;
import android.util.AttributeSet;

import com.vorknetwork.vork.R;
import com.vorknetwork.vork.flow.FramePathContainerView;
import com.vorknetwork.vork.ui.main.MainPathContainer;

import flow.path.Path;
import flow.path.PathContainer;

public abstract class MortarPathContainerView extends FramePathContainerView {

    public MortarPathContainerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected final PathContainer getContainer() {
        return new MainPathContainer(R.id.screen_switcher_tag,
                Path.contextFactory(new BasicMortarContextFactory(new ScreenScoper())), getScreenListener());
    }

    protected abstract MainPathContainer.ScreenListener getScreenListener();
}
