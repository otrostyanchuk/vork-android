package com.vorknetwork.vork.mortal;

public interface ScreenComponentFactory<T> {

    Object createComponent(T parent);
}
