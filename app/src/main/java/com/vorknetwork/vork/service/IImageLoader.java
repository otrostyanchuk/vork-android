package com.vorknetwork.vork.service;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;

public interface IImageLoader {
    int INVALID_RES_ID = 0;

    void displayImage(ImageView view, LoadingBuilder loadingBuilder);

    interface OnLoadingProcessListener {
        void onLoadingStarted(View view);

        void onLoadingFailed(View view);

        void onLoadingComplete(View view);

        void onLoadingCancelled(View view);
    }

    final class LoadingBuilder {
        String url;
        Uri uri;
        int placeHolder;
        Drawable placeHolderDrawable;
        OnLoadingProcessListener loadingProcessListener;
        ImageBounds outputBounds;

        public Drawable getPlaceHolderDrawable() {
            return placeHolderDrawable;
        }

        public LoadingBuilder setPlaceHolderDrawable(Drawable placeHolderDrawable) {
            this.placeHolderDrawable = placeHolderDrawable;
            return this;
        }

        public LoadingBuilder setUrl(String url) {
            this.url = url;
            return this;
        }

        public LoadingBuilder setUri(Uri uri) {
            this.uri = uri;
            return this;
        }

        public LoadingBuilder setPlaceHolder(int placeHolder) {
            this.placeHolder = placeHolder;
            return this;
        }

        public LoadingBuilder setLoadingProcessListener(OnLoadingProcessListener loadingProcessListener) {
            this.loadingProcessListener = loadingProcessListener;
            return this;
        }

        public LoadingBuilder setOutputBounds(ImageBounds outputBounds) {
            this.outputBounds = outputBounds;
            return this;
        }

        public String getUrl() {
            return url;
        }

        public Uri getUri() {
            return uri;
        }

        public int getPlaceHolder() {
            return placeHolder;
        }

        public OnLoadingProcessListener getLoadingProcessListener() {
            return loadingProcessListener;
        }

        public ImageBounds getOutputBounds() {
            return outputBounds;
        }
    }

    final class ImageBounds {
        protected int height = -1;
        protected int width = -1;

        public ImageBounds(int width, int height) {
            this.height = height;
            this.width = width;
        }

        public int getHeight() {
            return height;
        }

        public int getWidth() {
            return width;
        }
    }
}
