package com.vorknetwork.vork.service.impl;

import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.vorknetwork.vork.service.IImageLoader;

import javax.inject.Inject;

public class PicassoImageLoaderImpl implements IImageLoader {

    Picasso picasso;

    @Inject
    public PicassoImageLoaderImpl(Context context) {
        picasso = new Picasso.Builder(context)
                .build();
    }

    @Override
    public void displayImage(ImageView view, IImageLoader.LoadingBuilder loadingBuilder) {
        picasso.cancelRequest(view);
        RequestCreator requestCreator = (loadingBuilder.getUri() != null) ? picasso.load(loadingBuilder.getUri()) : picasso.load(loadingBuilder.getUrl());
        if (loadingBuilder.getOutputBounds() != null) {
            requestCreator = requestCreator.resize(loadingBuilder.getOutputBounds().getWidth(), loadingBuilder.getOutputBounds().getHeight()).centerInside();
        }
        if (loadingBuilder.getPlaceHolderDrawable() != null) {
            requestCreator.placeholder(loadingBuilder.getPlaceHolderDrawable());
        } else {
            if (loadingBuilder.getPlaceHolder() != IImageLoader.INVALID_RES_ID) {
                requestCreator.placeholder(loadingBuilder.getPlaceHolder());
            }
        }
        requestCreator.into(view);
    }
}
