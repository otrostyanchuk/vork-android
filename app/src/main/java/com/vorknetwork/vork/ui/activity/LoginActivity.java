package com.vorknetwork.vork.ui.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.vorknetwork.vork.R;
import com.vorknetwork.vork.VorkController;
import com.vorknetwork.vork.databinding.ActivityLoginBinding;
import com.vorknetwork.vork.ui.BaseActivity;

public class LoginActivity extends BaseActivity {

    private ActivityLoginBinding bindingObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindingObject = DataBindingUtil.setContentView(this, R.layout.activity_login);
        VorkController.getComponent().inject(this);


    }
}
