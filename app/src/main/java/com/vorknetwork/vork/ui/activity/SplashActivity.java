package com.vorknetwork.vork.ui.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.vorknetwork.vork.R;
import com.vorknetwork.vork.VorkController;
import com.vorknetwork.vork.databinding.ActivitySplashBinding;
import com.vorknetwork.vork.service.IBroadcast;
import com.vorknetwork.vork.service.eventbus.LiAuthSuccessEvent;
import com.vorknetwork.vork.ui.BaseActivity;
import com.vorknetwork.vork.ui.main.MainActivity;

import org.greenrobot.eventbus.Subscribe;

import javax.inject.Inject;

public class SplashActivity extends BaseActivity {

    private ActivitySplashBinding bindingObject;

    @Inject
    IBroadcast broadcast;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindingObject = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        VorkController.getComponent().inject(this);

        startActivity(new Intent(SplashActivity.this, MainActivity.class));
    }

    @Override
    protected void onPause() {
        super.onPause();
        broadcast.unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        broadcast.register(this);
    }

    @Subscribe
    public void handleLiProfileLoadEvent(LiAuthSuccessEvent event) {
        broadcast.removeStickyEvent(LiAuthSuccessEvent.class);
    }
}
