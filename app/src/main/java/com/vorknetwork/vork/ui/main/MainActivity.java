package com.vorknetwork.vork.ui.main;

import android.app.ActionBar;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.MenuItem;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.vorknetwork.vork.DaggerService;
import com.vorknetwork.vork.R;
import com.vorknetwork.vork.VorkController;
import com.vorknetwork.vork.databinding.ActivityMainBinding;
import com.vorknetwork.vork.flow.GsonParceler;
import com.vorknetwork.vork.flow.Tab;
import com.vorknetwork.vork.ui.BaseActivity;

import flow.Flow;
import flow.FlowDelegate;
import flow.History;
import flow.path.Path;
import io.fabric.sdk.android.Fabric;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

public class MainActivity extends BaseActivity implements Flow.Dispatcher {

    private MortarScope mortarScope;
    private FlowDelegate flowDelegate;

    private ActivityMainBinding bindingObject;

    private MainPathContainer.ScreenListener screenListener = new MainPathContainer.ScreenListener() {

        @Override
        public void onScreenSelected(MainPathScreen screen) {
            Tab currTab = Tab.values()[bindingObject.bottomNavigation.getCurrentItem()];
            if (currTab != screen.getTab()) {
                bindingObject.bottomNavigation.setCurrentItem(screen.getTab().ordinal(), false);
            }
        }
    };

    @Override
    public Object getSystemService(@NonNull String name) {
        if (getApplication() == null) {
            return super.getSystemService(name);
        }

        Object service = null;
        if (flowDelegate != null) {
            service = flowDelegate.getSystemService(name);
        }

        if (service == null && mortarScope != null && mortarScope.hasService(name)) {
            service = mortarScope.getService(name);
        }

        return service != null ? service : super.getSystemService(name);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mortarScope = MortarScope.findChild(getApplicationContext(), getClass().getName());

        if (mortarScope == null) {
            mortarScope = MortarScope.buildChild(getApplicationContext())
                    .withService(BundleServiceRunner.SERVICE_NAME, new BundleServiceRunner())
                    .withService(DaggerService.SERVICE_NAME, VorkController.getComponent()
                            .join(new MainActivityModule(this, screenListener)))
                    .build(getClass().getName());
        }
        DaggerService.<MainActivityComponent>getDaggerComponent(this).inject(this);

        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        FlowDelegate.NonConfigurationInstance nonConfig =
                (FlowDelegate.NonConfigurationInstance) getLastNonConfigurationInstance();
        GsonParceler parceler = new GsonParceler(new Gson());
//        flowDelegate = FlowDelegate.onCreate(nonConfig, getIntent(), savedInstanceState, parceler,
//                null, this);

        bindingObject = DataBindingUtil.setContentView(this, R.layout.activity_main);

        setupBottomBar();
    }

    private void setupBottomBar() {

        bindingObject.bottomNavigation.addItem(new AHBottomNavigationItem(getString(R.string.tab_members),
                R.color.colorPrimary));
        bindingObject.bottomNavigation.addItem(new AHBottomNavigationItem(getString(R.string.tab_chats),
                R.color.colorPrimary));
        bindingObject.bottomNavigation.addItem(new AHBottomNavigationItem(getString(R.string.tab_cards),
                R.color.colorPrimary));
        bindingObject.bottomNavigation.addItem(new AHBottomNavigationItem(getString(R.string.tab_add_card),
                R.color.colorPrimary));
        bindingObject.bottomNavigation.addItem(new AHBottomNavigationItem(getString(R.string.tab_settings),
                R.color.colorPrimary));

        bindingObject.bottomNavigation.setDefaultBackgroundColor(ActivityCompat.getColor(this, R.color.colorPrimary));
        bindingObject.bottomNavigation.setAccentColor(ActivityCompat.getColor(this, R.color.colorAccent));
        bindingObject.bottomNavigation.setInactiveColor(ActivityCompat.getColor(this, android.R.color.white));
    }

    @Override
    protected void onResume() {
        super.onResume();
//        flowDelegate.onResume();
    }

    @Override
    protected void onPause() {
//        flowDelegate.onPause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (isFinishing()) {
            MortarScope activityScope = MortarScope.findChild(getApplicationContext(), getClass().getName());
            if (activityScope != null) {
                activityScope.destroy();
            }
        }
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        BundleServiceRunner.getBundleServiceRunner(this).onSaveInstanceState(outState);
//        flowDelegate.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (flowDelegate.onBackPressed()) return;

        moveTaskToBack(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
    }

    @Override
    public void dispatch(Flow.Traversal traversal, final Flow.TraversalCallback callback) {
        Path path = traversal.destination.top();
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            boolean canGoBack = traversal.destination.size() > 1;
            actionBar.setDisplayHomeAsUpEnabled(canGoBack);
            actionBar.setHomeButtonEnabled(canGoBack);
        }

        bindingObject.basicActivityFrame.dispatch(traversal, new Flow.TraversalCallback() {
            @Override
            public void onTraversalCompleted() {
                invalidateOptionsMenu();
                callback.onTraversalCompleted();
            }
        });
    }
}
