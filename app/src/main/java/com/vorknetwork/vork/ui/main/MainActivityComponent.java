package com.vorknetwork.vork.ui.main;

import com.vorknetwork.vork.ApplicationComponent;
import com.vorknetwork.vork.DaggerScope;

@dagger.Subcomponent(modules = MainActivityModule.class)
@DaggerScope(ApplicationComponent.class)
public interface MainActivityComponent {


    void inject(MainActivity activity);

    void inject(MainView mainView);
}
