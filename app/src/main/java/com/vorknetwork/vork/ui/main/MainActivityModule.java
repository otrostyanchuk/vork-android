package com.vorknetwork.vork.ui.main;

import com.vorknetwork.vork.ui.BaseActivity;

import dagger.Module;
import dagger.Provides;
import flow.Flow;

@Module
public class MainActivityModule {

    private BaseActivity baseActivity;
    private final MainPathContainer.ScreenListener screenListener;

    public MainActivityModule(BaseActivity activity, MainPathContainer.ScreenListener screenListener) {
        this.baseActivity = activity;
        this.screenListener = screenListener;
    }

    @Provides
    BaseActivity providesActivity() {
        return baseActivity;
    }

    @Provides
    public MainPathContainer.ScreenListener provideScreenListener() {
        return screenListener;
    }

    @Provides
    public Flow provideFlow() {
        return Flow.get(baseActivity);
    }
}
