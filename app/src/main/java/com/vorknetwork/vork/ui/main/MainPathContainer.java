package com.vorknetwork.vork.ui.main;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vorknetwork.vork.flow.Layout;
import com.vorknetwork.vork.utils.FlowUtils;

import java.util.LinkedHashMap;
import java.util.Map;

import flow.Flow;
import flow.path.Path;
import flow.path.PathContainer;
import flow.path.PathContext;
import flow.path.PathContextFactory;

import static flow.Flow.Direction.BACKWARD;
import static flow.Flow.Direction.REPLACE;

public class MainPathContainer extends PathContainer {

    public interface ScreenListener {
        void onScreenSelected(MainPathScreen screen);
    }

    private static final Map<Class, Integer> PATH_LAYOUT_CACHE = new LinkedHashMap<>();
    private final PathContextFactory contextFactory;

    private final ScreenListener screenListener;

    public MainPathContainer(int tagKey, PathContextFactory contextFactory, ScreenListener screenListener) {
        super(tagKey);
        this.contextFactory = contextFactory;
        this.screenListener = screenListener;
    }

    @Override
    protected void performTraversal(final ViewGroup containerView,
                                    final TraversalState traversalState, Flow.Direction direction,
                                    final Flow.TraversalCallback callback) {
        final PathContext context;
        final PathContext oldPath;
        if (containerView.getChildCount() > 0) {
            oldPath = PathContext.get(containerView.getChildAt(0).getContext());
        } else {
            oldPath = PathContext.root(containerView.getContext());
        }

        MainPathScreen toPath = (MainPathScreen) traversalState.toPath();
        MainPathScreen fromPath = (MainPathScreen) traversalState.fromPath();

        View newView;
        context = PathContext.create(oldPath, toPath, contextFactory);
        int layout = getLayout(toPath);
        newView = LayoutInflater.from(context)
                .cloneInContext(context)
                .inflate(layout, containerView, false);

        View fromView = null;
        if (fromPath != null) {
            fromView = containerView.getChildAt(0);
            traversalState.saveViewState(fromView);
        }
        traversalState.restoreViewState(newView);

        if (fromView == null || direction == REPLACE) {
            containerView.removeAllViews();
            containerView.addView(newView);
            oldPath.destroyNotIn(context, contextFactory);
            callback.onTraversalCompleted();
        } else {
            containerView.addView(newView);
            final View finalFromView = fromView;
            final Flow.Direction requestedDirection = (fromPath.getTab() != toPath.getTab()) ?
                    REPLACE : direction;
            FlowUtils.waitForMeasure(newView, new FlowUtils.OnMeasuredCallback() {
                @Override
                public void onMeasured(View view, int width, int height) {
                    runAnimation(containerView, finalFromView, view, requestedDirection, new Flow.TraversalCallback() {
                        @Override
                        public void onTraversalCompleted() {
                            containerView.removeView(finalFromView);
                            oldPath.destroyNotIn(context, contextFactory);
                            callback.onTraversalCompleted();
                        }
                    });
                }
            });
        }
        if (screenListener != null) {
            screenListener.onScreenSelected(toPath);
        }
    }

    protected int getLayout(Path path) {
        Class pathType = path.getClass();
        Integer layoutResId = PATH_LAYOUT_CACHE.get(pathType);
        if (layoutResId == null) {
            Layout layout = (Layout) pathType.getAnnotation(Layout.class);
            if (layout == null) {
                throw new IllegalArgumentException(
                        String.format("@%s annotation not found on class %s", Layout.class.getSimpleName(),
                                pathType.getName()));
            }
            layoutResId = layout.value();
            PATH_LAYOUT_CACHE.put(pathType, layoutResId);
        }
        return layoutResId;
    }

    private void runAnimation(final ViewGroup container, final View from, final View to,
                              Flow.Direction direction, final Flow.TraversalCallback callback) {
        Animator animator = createSegue(from, to, direction);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                container.removeView(from);
                callback.onTraversalCompleted();
            }
        });
        animator.start();
    }

    private Animator createSegue(View from, View to, Flow.Direction direction) {
        AnimatorSet set = new AnimatorSet();
        switch (direction) {
            case REPLACE:
                set.play(ObjectAnimator.ofFloat(from, View.ALPHA, 1, 0));
                set.play(ObjectAnimator.ofFloat(to, View.ALPHA, 0, 1));
                return set;
            default:
                boolean backward = direction == BACKWARD;
                int fromTranslation = backward ? from.getWidth() : -from.getWidth();
                int toTranslation = backward ? -to.getWidth() : to.getWidth();
                set.play(ObjectAnimator.ofFloat(from, View.TRANSLATION_X, fromTranslation));
                set.play(ObjectAnimator.ofFloat(to, View.TRANSLATION_X, toTranslation, 0));
                return set;
        }
    }
}
