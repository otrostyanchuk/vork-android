package com.vorknetwork.vork.ui.main;


import com.vorknetwork.vork.flow.Tab;
import com.vorknetwork.vork.mortal.ScreenComponentFactory;

import flow.path.Path;

public abstract class MainPathScreen extends Path implements ScreenComponentFactory<MainActivityComponent> {

    public abstract Tab getTab();
}
