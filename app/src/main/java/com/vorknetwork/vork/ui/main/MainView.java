package com.vorknetwork.vork.ui.main;

import android.content.Context;
import android.util.AttributeSet;

import com.vorknetwork.vork.DaggerService;
import com.vorknetwork.vork.mortal.MortarPathContainerView;

import javax.inject.Inject;

public class MainView extends MortarPathContainerView {

    @Inject
    MainPathContainer.ScreenListener screenListener;

    public MainView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (isInEditMode())
            return;
        DaggerService.<MainActivityComponent>getDaggerComponent(context).inject(this);
    }

    @Override
    protected MainPathContainer.ScreenListener getScreenListener() {
        return screenListener;
    }
}
